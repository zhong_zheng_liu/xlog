// anaip
package qqwry

import (
	"fmt"
	"os"
	"strings"

	"golang.org/x/text/encoding/simplifiedchinese"
)

const (
	ipdatabase = "/var/PROGRAM/MANAGEMENT/modules/xbash/qqwry.dat"
)

func PathExists() (bool, error) {
	_, err := os.Stat(ipdatabase)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func init() {
	//加载dat文件到内存
	if ok, _ := PathExists(); ok {
		IPData.FilePath = ipdatabase
		res := IPData.InitIPData()
		if v, ok := res.(error); ok {
			//log.Panic(v)
			fmt.Println(v)
		}
	}
}

type Areainfo struct {
	Province    string
	City        string
	NetOperator string
}

func utf8togbk(s string) string {
	enc := simplifiedchinese.GBK.NewEncoder()
	trstr, _ := enc.String(s)
	return trstr
}

var shengstr string = utf8togbk("省")
var beijing string = utf8togbk("北京")
var shanghai string = utf8togbk("上海")
var tianjin string = utf8togbk("天津")
var chongqing string = utf8togbk("重庆")
var neimenggu string = utf8togbk("内蒙古")
var xizang string = utf8togbk("西藏")
var xinjiang string = utf8togbk("新疆")
var guangxi string = utf8togbk("广西")
var ningxia string = utf8togbk("宁夏")

func (q *QQwry) Anaxiptoarea(xrip string) (a *Areainfo) {
	var sheng, shi, netopt string
	if strings.Contains(xrip, ":") { //有些xff 里有包含端口
		xrip = strings.Split(xrip, ":")[0]
	}
	rr := q.Find(strings.TrimSpace(xrip))
	country := rr.Country
	netopt = rr.Area
	switch {
	case strings.Contains(country, shengstr):
		sheng = strings.Split(country, shengstr)[0] + shengstr
		shi = strings.Split(country, shengstr)[1]
	case strings.Contains(country, beijing):
		sheng = beijing
		shi = beijing
	case strings.Contains(country, shanghai):
		sheng = shanghai
		shi = shanghai
	case strings.Contains(country, chongqing):
		sheng = chongqing
		shi = chongqing
	case strings.Contains(country, tianjin):
		sheng = tianjin
		shi = tianjin
	case strings.Contains(country, neimenggu):
		sheng = neimenggu
		shi = strings.Trim(country, neimenggu)
	case strings.Contains(country, xizang):
		sheng = xizang
		shi = strings.Trim(country, xizang)
	case strings.Contains(country, xinjiang):
		sheng = xinjiang
		shi = strings.Trim(country, xinjiang)
	case strings.Contains(country, guangxi):
		sheng = guangxi
		shi = strings.Trim(country, guangxi)
	case strings.Contains(country, ningxia):
		sheng = ningxia
		shi = strings.Trim(country, ningxia)
	default:
		sheng = country
		shi = "-"
	}
	a = &Areainfo{
		Province:    sheng,
		City:        shi,
		NetOperator: netopt,
	}
	return
}
