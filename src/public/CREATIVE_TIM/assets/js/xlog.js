var dashboard = {};

//状态码占比
dashboard.getCodeDist = function () {
var codedistChart = echarts.init(document.getElementById('codedist'));
option = null;
$.get('/infodist?key=statuscode' + location.search,function(data) {
console.log(data);
codedistChart.setOption(option = {
    title : {
        text: '状态码占比',
        //subtext: '纯属虚构',
        x:'right'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: data.xname
    },
    series : [
        {
            name: '状态码占比',
            type: 'pie',
            radius : '75%',
            center: ['57%', '60%'],
            data:data.subvalue,
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
});
});
}

//时间占比
dashboard.getTimeDist = function () {
var timedistChart = echarts.init(document.getElementById('timedist'));
option = null;
$.get('/infodist?key=reqtime' + location.search,function(data) {
console.log(data);
timedistChart.setOption(option = {
    title : {
        text: '时间占比',
        //subtext: '纯属虚构',
        x:'right'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: data.xname
	},
    series : [
        {
            name: '时间占比',
            type: 'pie',
            radius : '75%',
            center: ['60%', '50%'],
            data:data.subvalue,
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
});
});
}

//请求方法占比
dashboard.getMethodist = function () {
var methoddistChart = echarts.init(document.getElementById('methoddist'));
option = null;
$.get('/infodist?key=method' + location.search,function(data) {
console.log(data);
methoddistChart.setOption(option = {
    title : {
        text: '请求方法占比',
        //subtext: '纯属虚构',
        x:'right'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: data.xname
	},
    series : [
        {
            name: '请求方式占比',
            type: 'pie',
            radius : '75%',
            center: ['60%', '50%'],
            data:data.subvalue,
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
});
});
}

//域名占比
dashboard.getHostdist = function () {
var hostdistChart = echarts.init(document.getElementById('hostdist'));
option = null;
$.get('/infodist?key=host' + location.search,function(data) {
console.log(data);
hostdistChart.setOption(option = {
    title : {
        text: '域名占比',
        //subtext: '纯属虚构',
        x:'left'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'right',
        data: data.xname
	},
    series : [
        {
            name: '域名占比',
            type: 'pie',
            radius : '60%',
            center: ['45%', '60%'],
            data:data.subvalue,
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
});
});
}

//平均时间走势	
dashboard.getTimeTrend = function () {
var timetrendChart = echarts.init(document.getElementById('timetrend'));
option = null;
$.get('/infotrend?key=time' + location.search,function(data) {
console.log(data);
timetrendChart.setOption(option = {
    title: {
        text: '时间走势[秒]'
    },
    tooltip: {
        trigger: 'axis',
		axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    legend: {
        data:["MaxTime","AverageTime"],
		selected: {
       'MaxTime': false
	    }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: data.dtime
    },
    yAxis: {
        type: 'value'
    },
    series: data.subvalue
});
});
}

//平均时间走势	
dashboard.getBodyByteTrend = function () {
var bytetrendChart = echarts.init(document.getElementById('bodybytetrend'));
option = null;
$.get('/infotrend?key=bodybyte' + location.search,function(data) {
console.log(data);
bytetrendChart.setOption(option = {
    title: {
        text: '流量走势[MB]'
    },
    tooltip: {
        trigger: 'axis',
		axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    legend: {
        data:["BodyBytes"],
		selected: {
       'MaxTime': false
	    }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: data.dtime
    },
    yAxis: {
        type: 'value'
    },
    series: data.subvalue
});
});
}

//状态码走势	
dashboard.getCodeTrend = function () {
var codetrendChart = echarts.init(document.getElementById('codetrend'));
option = null;
$.get('/infotrend?key=statuscode&codelist=total,200,30x,40x,50x' + location.search,function(data) {
console.log(data);
codetrendChart.setOption(option = {
    title: {
        text: '状态码走势'
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:["total","200","30x","40x","50x"],
		selected: {
            'total': false,
            '200': false
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: data.dtime
    },
    yAxis: {
        type: 'value'
    },
    series: data.subvalue
});
});
}

//错误状态码走势	
dashboard.getCodeTrendErr = function () {
var codetrenderrChart = echarts.init(document.getElementById('codetrenderr'));
option = null;
$.get('/infotrend?key=statuscode&codelist=400,403,404,499,500,502,503,504' + location.search,function(data) {
console.log(data);
codetrenderrChart.setOption(option = {
    title: {
        text: '异常状态码走势'
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:["400","403","404","499","500","502","503","504"]
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: data.dtime
    },
    yAxis: {
        type: 'value'
    },
    series: data.subvalue
});
});
}

//正常状态码走势	
dashboard.getCodeTrendok = function () {
var codetrendokChart = echarts.init(document.getElementById('codetrendok'));
option = null;
$.get('/infotrend?key=statuscode&codelist=total,200,301,302' + location.search,function(data) {
console.log(data);
codetrendokChart.setOption(option = {
    title: {
        text: '正常状态码走势'
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:["total","200","301","302"]
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: data.dtime
    },
    yAxis: {
        type: 'value'
    },
    series: data.subvalue
});
});
}

//爬虫走势	
dashboard.getSpiderTrend = function () {
var SpidertrendChart = echarts.init(document.getElementById('spidertrend'));
option = null;
$.get('/infotrend?key=useragent&ualist=baiduspider,spider360,sogouspider,googlebot,sosospider,yisouSpider' + location.search,function(data) {
console.log(data);
SpidertrendChart.setOption(option = {
    title: {
        text: '爬虫走势'
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:["Baiduspider","Spider360","Sogouspider","Googlebot","Sosospider","YisouSpider"]
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: data.dtime
    },
    yAxis: {
        type: 'value'
    },
    series: data.subvalue
});
});
}

//浏览器走势	
dashboard.getBrowserTrend = function () {
var BrowsertrendChart = echarts.init(document.getElementById('browsertrend'));
option = null;
$.get('/infotrend?key=useragent&ualist=msie,chrome,firefox,safari,otherua,total' + location.search,function(data) {
console.log(data);
BrowsertrendChart.setOption(option = {
    title: {
        text: '浏览器走势'
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:["msie","chrome","firefox","safari","OtherUa","total"],
		selected: {
            'total': false,
            'OtherUa': false
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: data.dtime
    },
    yAxis: {
        type: 'value'
    },
    series: data.subvalue
});
});
}

//移动浏览器走势	
dashboard.getWapBrowserTrend = function () {
var WapBrowsertrendChart = echarts.init(document.getElementById('wapbrowsertrend'));
option = null;
$.get('/infotrend?key=useragent&ualist=ucbrowser,micromessenger,mqqbrowser' + location.search,function(data) {
console.log(data);
WapBrowsertrendChart.setOption(option = {
    title: {
        text: '移动浏览器走势'
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:["UCBrowser","MicroMessenger","MQQBrowser"]
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: data.dtime
    },
    yAxis: {
        type: 'value'
    },
    series: data.subvalue
});
});
}

//获取ua百分比列表
dashboard.getUaDistPercent = function () {
$.get('/infodist?key=useragent&percent=true' + location.search,function(data) {
console.log(data);
var html="";
var jsondatas = eval(data);
$.each(data,function(i,item){
	html +="<tr><td>"+jsondatas[i].value+"</td><td>"+jsondatas[i].percent+"%</td><td>"+jsondatas[i].name+"</td></tr>";
});
$("#uadistpercent").append(html);
});
}

//过滤异常ua百分比列表
dashboard.getUaExceptDistPercent = function () {
$.get('/infodist?key=filteruseragent&percent=true' + location.search,function(data) {
console.log(data);
var html="";
var jsondatas = eval(data);
$.each(data,function(i,item){
	html +="<tr><td>"+jsondatas[i].value+"</td><td>"+jsondatas[i].percent+"%</td><td>"+jsondatas[i].name+"</td></tr>";
});
$("#uaexceptdistpercent").append(html);
});
}

//获取remoteaddress百分比列表
dashboard.getRaDistPercent = function () {
$.get('/infodist?key=remoteaddress&percent=true' + location.search,function(data) {
console.log(data);
var html="";
var jsondatas = eval(data);
$.each(data,function(i,item){
	html +="<tr><td>"+jsondatas[i].value+"</td><td>"+jsondatas[i].percent+"%</td><td>"+jsondatas[i].name+"</td></tr>";
});
$("#radistpercent").append(html);
});
}

//获取xff百分比列表
dashboard.getXffDistPercent = function () {
$.get('/infodist?key=xff&percent=true' + location.search,function(data) {
console.log(data);
var html="";
var jsondatas = eval(data);
$.each(data,function(i,item){
	html +="<tr><td>"+jsondatas[i].value+"</td><td>"+jsondatas[i].percent+"%</td><td>"+jsondatas[i].name+"</td></tr>";
});
$("#xffdistpercent").append(html);
});
}

//获取requesturi百分比列表
dashboard.getReqUriDistPercent = function () {
$.get('/infodist?key=requri&percent=true' + location.search,function(data) {
console.log(data);
var html="";
var jsondatas = eval(data);
$.each(data,function(i,item){
	html +="<tr><td>"+jsondatas[i].value+"</td><td>"+jsondatas[i].percent+"%</td><td>"+jsondatas[i].name+"</td></tr>";
});
$("#requridistpercent").append(html);
});
}

//获取rawturi百分比列表
dashboard.getRawUriDistPercent = function () {
$.get('/infodist?key=rawuri&percent=true' + location.search,function(data) {
console.log(data);
var html="";
var jsondatas = eval(data);
$.each(data,function(i,item){
	html +="<tr><td>"+jsondatas[i].value+"</td><td>"+jsondatas[i].percent+"%</td><td>"+jsondatas[i].name+"</td></tr>";
});
$("#rawuridistpercent").append(html);
});
}

//获取uripattern百分比列表
dashboard.getUriPtDistPercent = function () {
$.get('/infodist?key=uript&percent=true' + location.search,function(data) {
console.log(data);
var html="";
var jsondatas = eval(data);
$.each(data,function(i,item){
	html +="<tr><td>"+jsondatas[i].value+"</td><td>"+jsondatas[i].percent+"%</td><td>"+jsondatas[i].name+"</td></tr>";
});
$("#requriptdistpercent").append(html);
});
}

//过滤异常rawuri百分比列表
dashboard.getRawUriExceptDistPercent = function () {
$.get('/infodist?key=filterrawuri&percent=true' + location.search,function(data) {
console.log(data);
var html="";
var jsondatas = eval(data);
$.each(data,function(i,item){
	html +="<tr><td>"+jsondatas[i].value+"</td><td>"+jsondatas[i].percent+"%</td><td>"+jsondatas[i].name+"</td></tr>";
});
$("#rawuriexceptdistpercent").append(html);
});
}

//获取Referer百分比列表
dashboard.getRefererDistPercent = function () {
$.get('/infodist?key=referer&percent=true' + location.search,function(data) {
console.log(data);
var html="";
var jsondatas = eval(data);
$.each(data,function(i,item){
	html +="<tr><td>"+jsondatas[i].value+"</td><td>"+jsondatas[i].percent+"%</td><td>"+jsondatas[i].name+"</td></tr>";
});
$("#rfdistpercent").append(html);
});
}

//获取Host字节百分比列表
dashboard.getByteHostDistPercent = function () {
$.get('/bodybytedist?key=host' + location.search,function(data) {
console.log(data);
var html="";
var jsondatas = eval(data);
$.each(data,function(i,item){
	html +="<tr><td>"+jsondatas[i].value+"</td><td>"+jsondatas[i].percent+"%</td><td>"+jsondatas[i].name+"</td></tr>";
});
$("#bytehostpercent").append(html);
});
}

//获取Uri字节百分比列表
dashboard.getByteRawuriDistPercent = function () {
$.get('/bodybytedist?key=rawuri' + location.search,function(data) {
console.log(data);
var html="";
var jsondatas = eval(data);
$.each(data,function(i,item){
	html +="<tr><td>"+jsondatas[i].value+"</td><td>"+jsondatas[i].percent+"%</td><td>"+jsondatas[i].name+"</td></tr>";
});
$("#byterawuripercent").append(html);
});
}

//获取Ip字节百分比列表
dashboard.getByteIpDistPercent = function () {
$.get('/bodybytedist?key=remoteaddress' + location.search,function(data) {
console.log(data);
var html="";
var jsondatas = eval(data);
$.each(data,function(i,item){
	html +="<tr><td>"+jsondatas[i].value+"</td><td>"+jsondatas[i].percent+"%</td><td>"+jsondatas[i].name+"</td></tr>";
});
$("#byteippercent").append(html);
});
}


//总流量
dashboard.getTotalByte = function () {
$.get('/totalinfo?key=totalbyte' + location.search,function(data) {
console.log(data);
var html="";
$.each(data,function(i,item){
	html +=item+"MB";
});
$("#totalbyte").html(html);
});
}

//总请求
dashboard.getTotalReq = function () {
$.get('/totalinfo?key=totalrequest' + location.search,function(data) {
console.log(data);
var html="";
$.each(data,function(i,item){
	html +=item;
});
$("#totalreq").html(html);
});
}

//平均时间
dashboard.getAvgReqTime = function () {
$.get('/totalinfo?key=avgreqtime' + location.search,function(data) {
console.log(data);
var html="";
$.each(data,function(i,item){
	html +=item+"s";
});
$("#avgreqtime").html(html);
});
}

//负载
dashboard.getLoad = function () {
$.get('/totalinfo?key=load' + location.search,function(data) {
console.log(data);
var html="";
$.each(data,function(i,item){
	html +=item;
});
$("#load").html(html);
});
}

//地图
dashboard.getChinaMaps = function () {
var chinaMapChart = echarts.init(document.getElementById('chinamap'));
option = null;
$.get('/mapdist' + location.search,function(data) {
console.log(data);
chinaMapChart.setOption(
option = {
    title : {
        text: '请求时间在全国范围的平均分布',
        subtext: '网速分布',
        x:'center'
    },
	mapValueCalculation: 'average', 
    tooltip : {
        trigger: 'item'
    },
    legend: {
        orient: 'vertical',
        x:'left',
        data:['电信','联通','移动','其他']
    },
    dataRange: {
        x: 'left',
        y: 'bottom',
        splitList: [
            {start: 10,label: '10秒以上', color: 'red'},
            {start: 3, end: 10, label: '3-10秒', color: '#FFC125'},
            {start: 0.001,end: 3,label: '小于3秒', color: 'green'},
			{end: 0.001,label: '没有数据', color: 'blue'}
        ],
        color: ['#E0022B', '#E09107', '#A3E00B']
    },
    toolbox: {
        show: true,
        orient : 'vertical',
        x: 'right',
        y: 'center',
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    series : [
        {
            name: '电信',
            type: 'map',
            mapType: 'china',
            roam: false,
            itemStyle:{
                normal:{
                    label:{
                        show:true,
                        textStyle: {
                           color: "rgb(249, 249, 249)"
                        }
                    }
                },
                emphasis:{label:{show:true}}
            },
            data:data.dxmapvalue
        },
		{
            name: '联通',
            type: 'map',
            mapType: 'china',
            roam: false,
            itemStyle:{
                normal:{
                    label:{
                        show:true,
                        textStyle: {
                           color: "rgb(249, 249, 249)"
                        }
                    }
                },
                emphasis:{label:{show:true}}
            },
            data:data.ltmapvalue
        },
		{
            name: '移动',
            type: 'map',
            mapType: 'china',
            roam: false,
            itemStyle:{
                normal:{
                    label:{
                        show:true,
                        textStyle: {
                           color: "rgb(249, 249, 249)"
                        }
                    }
                },
                emphasis:{label:{show:true}}
            },
            data:data.ydmapvalue
        },
		{
            name: '其他',
            type: 'map',
            mapType: 'china',
            roam: false,
            itemStyle:{
                normal:{
                    label:{
                        show:true,
                        textStyle: {
                           color: "rgb(249, 249, 249)"
                        }
                    }
                },
                emphasis:{label:{show:true}}
            },
            data:data.othermapvalue
        }
    ]
});
});	
}


dashboard.getAll = function () {
    for (var item in dashboard.fnMap) {
        if (dashboard.fnMap.hasOwnProperty(item) && item !== "all") {
            dashboard.fnMap[item]();
        }
    }
}

dashboard.fnMap = {
    all: dashboard.getAll,
    codedist: dashboard.getCodeDist,
    timedist: dashboard.getTimeDist,
    timetrend: dashboard.getTimeTrend,
    codetrend: dashboard.getCodeTrend,
    uatrend: dashboard.getUaTrend,
};

//$(function(){
//	dashboard.getCodeTrend();
//});
