package common

//此部分主要统计中国地图的访问速度部分

import (
	"encoding/json"
	"fmt"
	"strings"

	"golang.org/x/text/encoding/simplifiedchinese"
)

type MapDist struct {
	Dxmapvalue    []DxArry    `json:"dxmapvalue"`
	Ltmapvalue    []LtArry    `json:"ltmapvalue"`
	Ydmapvalue    []YdArry    `json:"ydmapvalue"`
	Othermapvalue []OtherArry `json:"othermapvalue"`
}

type DxArry struct {
	Name  string  `json:"name"`
	Value float64 `json:"value"`
}

type LtArry struct {
	Name  string  `json:"name"`
	Value float64 `json:"value"`
}

type YdArry struct {
	Name  string  `json:"name"`
	Value float64 `json:"value"`
}

type OtherArry struct {
	Name  string  `json:"name"`
	Value float64 `json:"value"`
}

var DxMap = make(map[string]float64)
var LtMap = make(map[string]float64)
var YdMap = make(map[string]float64)
var OtherMap = make(map[string]float64)

//存放ip及请求时间的map
var XipSecMap = make(map[string]float64)

var AreaMap = make(map[string]float64)

//存放ip和地区的缓存数据
var IpdataMap = make(map[string]string)

func SetAreaMap(region, yys string, timesec float64) {
	if yys == "dianxin" {
		AreaMap = DxMap
	} else if yys == "liantong" {
		AreaMap = LtMap
	} else if yys == "yidong" {
		AreaMap = YdMap
	} else {
		AreaMap = OtherMap
	}
	switch {
	case strings.Contains(region, "北京"):
		AreaMap["北京"] = timesec
	case strings.Contains(region, "天津"):
		AreaMap["天津"] = timesec
	case strings.Contains(region, "上海"):
		AreaMap["上海"] = timesec
	case strings.Contains(region, "重庆"):
		AreaMap["重庆"] = timesec
	case strings.Contains(region, "河北"):
		AreaMap["河北"] = timesec
	case strings.Contains(region, "河南"):
		AreaMap["河南"] = timesec
	case strings.Contains(region, "云南"):
		AreaMap["云南"] = timesec
	case strings.Contains(region, "辽宁"):
		AreaMap["辽宁"] = timesec
	case strings.Contains(region, "黑龙江"):
		AreaMap["黑龙江"] = timesec
	case strings.Contains(region, "湖南"):
		AreaMap["湖南"] = timesec
	case strings.Contains(region, "安徽"):
		AreaMap["安徽"] = timesec
	case strings.Contains(region, "山东"):
		AreaMap["山东"] = timesec
	case strings.Contains(region, "新疆"):
		AreaMap["新疆"] = timesec
	case strings.Contains(region, "江苏"):
		AreaMap["江苏"] = timesec
	case strings.Contains(region, "浙江"):
		AreaMap["浙江"] = timesec
	case strings.Contains(region, "江西"):
		AreaMap["江西"] = timesec
	case strings.Contains(region, "湖北"):
		AreaMap["湖北"] = timesec
	case strings.Contains(region, "广西"):
		AreaMap["广西"] = timesec
	case strings.Contains(region, "甘肃"):
		AreaMap["甘肃"] = timesec
	case strings.Contains(region, "山西"):
		AreaMap["山西"] = timesec
	case strings.Contains(region, "内蒙古"):
		AreaMap["内蒙古"] = timesec
	case strings.Contains(region, "陕西"):
		AreaMap["陕西"] = timesec
	case strings.Contains(region, "吉林"):
		AreaMap["吉林"] = timesec
	case strings.Contains(region, "福建"):
		AreaMap["福建"] = timesec
	case strings.Contains(region, "贵州"):
		AreaMap["贵州"] = timesec
	case strings.Contains(region, "广东"):
		AreaMap["广东"] = timesec
	case strings.Contains(region, "青海"):
		AreaMap["青海"] = timesec
	case strings.Contains(region, "西藏"):
		AreaMap["西藏"] = timesec
	case strings.Contains(region, "四川"):
		AreaMap["四川"] = timesec
	case strings.Contains(region, "宁夏"):
		AreaMap["宁夏"] = timesec
	case strings.Contains(region, "海南"):
		AreaMap["海南"] = timesec
	case strings.Contains(region, "台湾"):
		AreaMap["台湾"] = timesec
	case strings.Contains(region, "香港"):
		AreaMap["香港"] = timesec
	case strings.Contains(region, "澳门"):
		AreaMap["澳门"] = timesec
	default:
	}
}

func CountChinaMap(xrip string, timesec float64, statuscode int64) {
	mutex.Lock()
	defer mutex.Unlock()
	//if !(strings.HasPrefix(xrip, "192.168.") || strings.HasPrefix(xrip, "172.16.") || strings.HasPrefix(xrip, "10.")) {
	if statuscode != 404 && statuscode != 503 && timesec > 0.5 {
		XipSecMap[xrip] = timesec
	}

	if statuscode != 404 && statuscode != 503 && timesec > 3 {
		if _, ok := Gt3IpMap[xrip]; ok {
			Gt3IpMap[xrip] = Gt3IpMap[xrip] + 1
		} else {
			Gt3IpMap[xrip] = 1
		}
	}

	//if (statuscode == 200 && timesec > 3) || statuscode != 200 || statuscode != 301 || statuscode != 302 || statuscode != 404 || statuscode != 503 {
	if _, ok := CountIpMap[xrip]; ok {
		CountIpMap[xrip] = CountIpMap[xrip] + 1
	} else {
		CountIpMap[xrip] = 1
	}
	//}
	//}
}

func gbktoutf8(s string) string {
	enc := simplifiedchinese.GBK.NewDecoder()
	trstr, _ := enc.String(s)
	return trstr
}

//返回中国地图的地区及请求时间
func PrintChinaMap() string {
	var operator string
	var count int
	mutex.Lock()
	defer mutex.Unlock()
	for xrip, timesec := range XipSecMap {
		//fmt.Println(xrip, timesec)
		count++
		//fmt.Println(count)
		if count > 250 {
			break
		}

		if value, ok := IpdataMap[xrip]; ok {
			operator = value
		} else {
			rr := qqW.Find(xrip)
			operator = gbktoutf8(rr.Country + " " + rr.Area)
			//country = rr.Country
			IpdataMap[xrip] = operator
		}
		switch {
		case strings.Contains(operator, "电信"):
			SetAreaMap(operator, "dianxin", timesec)
		case strings.Contains(operator, "联通"):
			SetAreaMap(operator, "liantong", timesec)
		case strings.Contains(operator, "移动"):
			SetAreaMap(operator, "yidong", timesec)
		default:
			SetAreaMap(operator, "other", timesec)
		}
	}

	var dxarr = make([]DxArry, 0)
	var ltarr = make([]LtArry, 0)
	var ydarr = make([]YdArry, 0)
	var otherarr = make([]OtherArry, 0)

	for k, v := range DxMap {
		dxarr = append(dxarr, *&DxArry{k, v})
	}

	for k, v := range LtMap {
		ltarr = append(ltarr, *&LtArry{k, v})
	}
	for k, v := range YdMap {
		ydarr = append(ydarr, *&YdArry{k, v})
	}
	for k, v := range OtherMap {
		otherarr = append(otherarr, *&OtherArry{k, v})
	}

	aaa := &MapDist{
		Dxmapvalue:    dxarr,
		Ltmapvalue:    ltarr,
		Ydmapvalue:    ydarr,
		Othermapvalue: otherarr,
	}

	jsonStr, err := json.Marshal(aaa)
	if err != nil {
		fmt.Println(err)
	}
	//fmt.Printf("%s", jsonStr)
	return fmt.Sprintf("%s", jsonStr)
}
